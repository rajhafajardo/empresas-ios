//
//  EnterpriseAPI.swift
//  empresasRajha
//
//  Created by Rajha Fajardo on 17/02/20.
//  Copyright © 2020 Rajha Fajardo. All rights reserved.
//

import Foundation

class EnterpriseAPI: APIRequest {
    
    @discardableResult
       
       static func getCompanies(enterprise_name: String, callback:ResponseBlock<[Enterprises]>?) ->EnterpriseAPI{
               
               let request = EnterpriseAPI(method: .get, path: "enterprises?&name="+enterprise_name, parameters: nil, urlParameters: nil, cacheOption: .networkOnly) { (response, error, cache) in
                   
                   if let error = error {
                       print(error.responseObject ?? "nil")
                   } else if let response = response as? [String: Any], let enterprisesJSON = response["enterprises"] as? [ [String: Any] ] {
                       var enterprises = [Enterprises]()
                       
                       for dic in enterprisesJSON {
                           let enterprise = Enterprises(dictionary: dic)
                           enterprises.append(enterprise)
                       }
                       callback?(enterprises, nil, cache)
                   }
               }
               request.shouldSaveInCache = false
               
               request.makeRequest()
               return request
    }
}
