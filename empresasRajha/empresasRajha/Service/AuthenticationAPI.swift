//
//  AuthenticationAPI.swift
//  empresasRajha
//
//  Created by Rajha Fajardo on 05/02/20.
//  Copyright © 2020 Rajha Fajardo. All rights reserved.
//

import Foundation
import UIKit

class AuthenticationAPI: APIRequest {
    
    override init(method: API.HTTPMethod, path: String, parameters: [String : Any]?, urlParameters: [String : Any]?, cacheOption: API.CacheOption, completion: ResponseBlock<Any>?) {
        super.init(method: method, path: path, parameters: parameters, urlParameters: urlParameters, cacheOption: cacheOption, completion: completion)
        
        self.baseURL = URL(string: "\(APIRequest.Constants.baseURL)/\(APIRequest.Constants.apiPath)/")!
    }
    
    @discardableResult
    static func loginWith(email: String, password: String, completion: ResponseBlock<Any>?) -> AuthenticationAPI {
       
        let request = AuthenticationAPI(method: .post, path: "users/auth/sign_in", parameters: ["email": email, "password": password], urlParameters: nil, cacheOption: .networkOnly) { (response, error, cache) in
            
            if let error = error {
                print(error.responseObject ?? "nil")
            } else {
                completion?(response, nil, cache)
            }
        }
        request.shouldSaveInCache = false
        request.makeRequest()
        
        return request
    }

}

