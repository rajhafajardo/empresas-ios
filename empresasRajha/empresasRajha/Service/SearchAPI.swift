//
//  SearchAPI.swift
//  empresasRajha
//
//  Created by Rajha Fajardo on 14/02/20.
//  Copyright © 2020 Rajha Fajardo. All rights reserved.
//
import Foundation

class SearchAPI: APIRequest {
    
    override init(method: API.HTTPMethod, path: String, parameters: [String : Any]?, urlParameters: [String : Any]?, cacheOption: API.CacheOption, completion: ResponseBlock<Any>?) {
        super.init(method: method, path: path, parameters: parameters, urlParameters: urlParameters, cacheOption: cacheOption, completion: completion)
        
        self.baseURL = URL(string: "\(APIRequest.Constants.baseURL)/\(APIRequest.Constants.apiPath)/")!
    }
    
    @discardableResult
    static func searchEnterprises(name: String, completion: ResponseBlock<Any>?) -> SearchAPI {
        //mmapeamento
  
        let request = SearchAPI(method: .get, path: "enterprises?&name=", parameters: ["name": name], urlParameters: nil, cacheOption: .networkOnly) { (response, error, cache) in
            
            if let error = error {
                print(error.responseObject ?? "nil")
            } else {
                completion?(response, nil, cache)
            }
        }
                    
        request.shouldSaveInCache = false
        request.makeRequest()
                    
        return request
    }
    
}

