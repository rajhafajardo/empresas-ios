//
//  SearchViewController.swift
//  empresasRajha
//
//  Created by Rajha Fajardo on 30/01/20.
//  Copyright © 2020 Rajha Fajardo. All rights reserved.
//
import UIKit

class SearchViewController: UIViewController {
    
  
    //MARK: -Outlets
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    var companies: [Enterprises] = [] {
        didSet {
            self.tableView.reloadData()
        }
    }
      
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //searchBar.delegate = self
        tableView.delegate = self
        tableView.dataSource = self
  }
}

extension SearchViewController : UISearchBarDelegate{
func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
    
    EnterpriseAPI.getCompanies(enterprise_name: searchBar.text!){ (response, error, cache) in
        if let response = response {
            self.companies = response
        } else if let error = error{
            if let urlResponse = error.urlResponse, urlResponse.statusCode == 401 {
        } else if let responseObject = error.responseObject as? [String: Any], let _ =
        responseObject["error_message"]{
        }
    }
    }
    }
}

    extension SearchViewController : UITableViewDelegate, UITableViewDataSource {
    
        
        
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return companies.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "EnterpriseCell", for: indexPath) as! EnterpriseTableViewCell
            
        let company = companies[indexPath.row]
            
        cell.EnterpriseName.text = company.enterprise_name
        cell.EnterpriseType.text = company.enterprise_type_name
        cell.EnterpriseCountry.text = company.country
        
        return cell
    }
}
