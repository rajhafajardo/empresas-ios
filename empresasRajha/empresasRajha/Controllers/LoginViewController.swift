//
//  LoginViewController.swift
//  empresasRajha
//
//  Created by Rajha Fajardo on 30/01/20.
//  Copyright © 2020 Rajha Fajardo. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        loginTextButton.layer.cornerRadius = 3
        // Do any additional setup after loading the view.
        
    }

    @IBOutlet weak var emailText: UITextField!
    
    @IBOutlet weak var passwordText: UITextField!
    
    @IBAction func loginButton(_ sender: Any) {
        AuthenticationAPI.loginWith(email: "testeapple@ioasys.com.br", password: "12341234") { (user, error, cache) in
           
            if user != nil {
                let searchViewController : UIViewController = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(identifier: "searchViewController")
                self.present(searchViewController, animated: true, completion: nil)
                
            }
//
//            }else{
//                let alertController = UIAlertController(title: "ERRO", message: "Login incorreto", preferredStyle: .alert)
//                alertController.addAction(UIAlertAction(title: "Cancelar", style: .default))
//                self.present(alertController, animated: true, completion: nil)
//            }
        
        }
        
    }
    
    @IBOutlet weak var loginTextButton: UIButton!
}
