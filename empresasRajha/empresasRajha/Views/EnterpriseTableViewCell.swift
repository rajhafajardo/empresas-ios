//
//  EnterpriseTableViewCell.swift
//  empresasRajha
//
//  Created by Rajha Fajardo on 30/01/20.
//  Copyright © 2020 Rajha Fajardo. All rights reserved.
//

import UIKit

class EnterpriseTableViewCell: UITableViewCell {
    
    @IBOutlet weak var entrepriseImage: UIImageView!
    @IBOutlet weak var EnterpriseName: UILabel!
    @IBOutlet weak var EnterpriseType: UILabel!
    @IBOutlet weak var EnterpriseCountry: UILabel!
    
    var entrepriseImg: UIImage? = UIImage()
    var name : String = ""
    var type : String = ""
    var country : String = ""

    func CellEnterprise () {
        entrepriseImage.image = entrepriseImg
        EnterpriseName.text = name
        
        
        
    }
    
}
