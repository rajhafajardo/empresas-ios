//
//  User.swift
//  empresasRajha
//
//  Created by Rajha Fajardo on 05/02/20.
//  Copyright © 2020 Rajha Fajardo. All rights reserved.
//

import Foundation


struct User: Mappable {
    
        
        var id: String
        var login: String
        var password: String
        
        init(mapper: Mapper) {
            self.id = mapper.keyPath("id")
            self.login = mapper.keyPath("login")
            self.password = mapper.keyPath("password")
            
        }
    }
