//
//  EnterpriseMap.swift
//  empresasRajha
//
//  Created by Rajha Fajardo on 03/02/20.
//  Copyright © 2020 Rajha Fajardo. All rights reserved.
//

import Foundation
import UIKit

struct Enterprises: Mappable {
    
    var enterprise_name: String = ""
    var photo: String? = ""
    var description: String? = ""
    var country: String? = ""
    var enterprise_type_name: String? = ""
    
    init(mapper: Mapper) {
        
        self.enterprise_name = mapper.keyPath("enterprise_name")
        self.photo = mapper.keyPath("photo")
        self.description = mapper.keyPath("description")
        self.country = mapper.keyPath("country")
        self.enterprise_type_name = mapper.keyPath("enterprise_type.enterprise_type_name")
    }
}

